const path = require('path');
const { name } = require('./config');
const { User, Product } = require('./models');
const CsvImporter = require('./services/csvImporter');

console.log(name);
const user = new User();
const product = new Product();

const DATA_PATH = path.resolve(__dirname, './data')

new CsvImporter().importChanges(DATA_PATH, console.log)