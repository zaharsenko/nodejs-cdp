const parse = require('csv-parse');
const parseCsvSync = require('csv-parse/lib/sync');
const { promisify } = require('util');
const Importer = require('./importer');

const parseCsv = promisify(parse)

class CsvImporter extends Importer {
  async _toJson (data) {
    return parseCsv(data)
  }

  _toJsonSync (data) {
    return parseCsvSync(data)
  }
}

module.exports = CsvImporter;